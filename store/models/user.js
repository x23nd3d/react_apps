const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    role: {
      type: String,
      default: 'user'
    },
    // TODO add datepicker
    password: {
        type: String,
        required: true
    },
    inviteID: {
      type: String,
      required:true
    },
    todo: {
        type: Schema.Types.ObjectId,
        ref: 'TodoApp',
    },
    last_login_time: {
        type: Date,
        default: null
    },
    avatar: {
        type: String,
        default: "https://html5css.ru/howto/img_avatar.png"
    },
    lang: {
        type: String,
        default: "en"
    }
});

userSchema.methods.recordLogin = function (date) {

    this.last_login_time = date;

    return this.save();
}


userSchema.methods.changeLang = function (lang) {

    this.lang = lang;

    return this.save();
}



module.exports = model('User', userSchema);