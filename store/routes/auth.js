const { Router } = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const auth = require('../middleware/auth');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const TodoModel = require('../models/todoModel');
const router = Router();


const checkUserInterface = (request, response) => {

    // const SHARED_ACCESS = ['46.219.66.66'];
    const SHARED_ACCESS = ['46.219.66.66', '176.98.31.15', '95.158.53.42', '88.155.17.237', '93.74.167.0', '89.252.16.15', '185.191.178.130'];
    // TODO add check every time so we could log out person who just used debugger to log in

    const IP = request.connection.remoteAddress.split('::ffff:')[1];
    const idx = SHARED_ACCESS.findIndex(ip => IP===ip);

    if (idx >= 0) {
        return response.status(201).json({status: "allowed_ip_rule"});
    } else {
        return response.status(201).json({status: "not_allowed_ip_rule"});
    }

}


const checkUser = async (userId) => {
    const user = await User.findOne({
        '_id': userId
    });

    const candidate = await User.findOne(user);

    return candidate;
}


router.get('/attempt', async (req, res) => {
    try {
        checkUserInterface(req, res);
    } catch (e) {
        console.log('Error', e)
    }
});

router.post('/signup', async (req, res) => {
    try {

        const IDs = ['testID', 'testID2'];

        const { name, surname, email, password } = req.body;
        let { inviteID } = req.body;
        console.log(req.body)
        const candidate = await User.findOne({ email });

        if (candidate) {
            console.log(candidate, 'CANDIDATE');
            res.json({status: "failure", text:"registered:true"});
        } else {
            if (!name.length) {
                return res.json({status: "failure", text:"noname"});
            }
            // TODO length of name, like do not use just spaces while register
            if (!surname.length) {
                return res.json({status: "failure", text:"nosurname"});
            }
            if (!password.length) {
                return res.json({status: "failure", text:"nopassword"});
            }
            if (!email.length) {
                return res.json({status: "failure", text:"noemail"});
            }

            if (IDs.indexOf(inviteID) >= 0) {
                const hashPassword = await bcrypt.hash(password, 10);

                const user = new User({
                    name,
                    surname,
                    email,
                    password: hashPassword,
                    inviteID,
                })
                await user.save();

                const todo = new TodoModel({
                    todo:{items:[]},
                    userId: user._id
                });

                await todo.save();

                res.status(200).json(user);
            } else {
                console.log('INVITE ID INCORRECT')
                return res.json({status: "failure", text:"INVITE ID INCORRECT"});
            }


        }

    } catch (e) {
        console.log('Error', e)
    }

});


router.post('/login', async (req, res) => {
    try {
        const {email, password} = req.body;
        console.log(req.body, 'LOGIN ATTEMPT')

        const candidate = await User.findOne({ email });

        if (candidate) {
            const areSame = await bcrypt.compare(password, candidate.password);

            if (areSame) {
                // TODO: DESTROY TOKEN THEN
                const token = jwt.sign({
                        userId: candidate.id
                    }, 'todoApplicationAPIGenerationDev',
                    {expiresIn: '3h'});
                res.json({ token, userId:candidate, status: 'Success' })
            } else {
                res.json({status: "failure", text:"datawrong"});
            }

        } else {
            res.json({status: "failure", text:"notfound"});
        }
    } catch (e) {
        console.log('Error', e)
    }
});

router.post('/check', auth, async (req, res) => {
    const candidate = await checkUser(req.jwtUserID);

    if (candidate) {
        // user is logged in

        res.json({status:'connected'})
    } else {
        // user was logged out
        res.json({status:'failure'})
    }
});

router.post('/chlang', auth, async (req, res) => {
    const candidate = await checkUser(req.jwtUserID);

    if (candidate) {

        const { language } = req.body;
        console.log(language, 'LANG');
        // user is logged in
        await candidate.changeLang(language);

        res.json({lang:candidate.lang})
    } else {
        // user was logged out
        res.json({status:'failure'})
    }
});

router.get('/user', auth, async (req, res) => {
    const candidate = await checkUser(req.jwtUserID);

    console.log(candidate, 'CANDIDATE CHECK', req.jwtUserID);

    if (candidate) {
        // user is logged in
        console.log('WE HAVE CANDIDAAAAAAAAATE', candidate)

        res.json({user:candidate})
    } else {
        // user was logged out
        res.json({user:'user_not_found'})
    }
});



router.post('/logout', auth,async (req, res) => {
    try {
        const candidate = await checkUser(req.jwtUserID);

        if (candidate) {
            console.log('Candidate found, implement function.')
            // user is logged in
            candidate.recordLogin(Date.now());
            res.json({status:'recorded'})
        } else {
            console.log('Candidate NOT found.')
            // user was logged out
            res.json({status:'failure'})
        }
    } catch (e) {
        console.log('Error', e)
    }
});


module.exports = router;