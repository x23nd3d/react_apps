import ProgressBar from "../components/ProgressBar/ProgressBar";
import React, {Component} from 'react';
import Stats from '../components/stats/Stats'
import stats from '../components/stats/Stats.module.scss';
import StatsLines from "../components/stats-lines/StatsLines";

export default class Dashboard extends Component {


    render() {

        const userData = {
            title: "Users",
            count: 459,
            daysCount: [7, 26, 55, 45, 4, 55, 12]
        }

        const incomeData = {
            title: "Income",
            count: '₴' + 95545,
            daysCount: [200, 1250, 407, 2500, 1680, 300, 450]
        }

        const graphs = incomeData.daysCount.map((item, i) => {
            const formula = item / 100 + 40
            return (
                <div key={i} className={stats['graph']} style={{height:`calc(${formula}px)`, backgroundColor:"red"}}></div>
            )
        });

        const { title, count, daysCount } = userData;

        const content =
            <div className="stats__field">
                <Stats title={title} count={count} data={daysCount}/>
                <Stats title={incomeData.title} count={incomeData.count} data={incomeData.daysCount}>
                    {graphs}
                </Stats>
                <StatsLines/>
                <Stats title={title} count={count} data={daysCount}/>
            </div>;

          return this.props.active ?  <ProgressBar>{content}</ProgressBar> : content;





            //  return(
            //   <>
            //          <ProgressBar>
            //             <div className="stats__field">
            //                 <Stats title={title} count={count} data={daysCount}/>
            //                  <Stats title={incomeData.title} count={incomeData.count} data={incomeData.daysCount}>
            //                     {graphs}
            //                 </Stats>
            //                 <StatsLines/>
            //               <Stats title={title} count={count} data={daysCount}/>
            //              </div>
            //         </ProgressBar>
            //     </>
            // )




    }
}

