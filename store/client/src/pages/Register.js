import React, {Component} from "react";
import Auth from "../services/auth";
import loginModule from '../components/login-panel/Login.module.scss'
const toastr = require('toastr');



export class Register extends Component {
    state = {
        register: {
            name: "",
            surname: "",
            email: "",
            password: "",
            passwordRepeat: "",
            avatar: "",
            inviteID: ""
        }
    }

    todoService = new Auth();



    setValue = (e) => {

        const value = e.target.value;
        // TODO correct UNDEFINED AND MAKE AS 1 FUNCTION

        const obj = {
            ...this.state.register,
            [e.target.name]: value,
            [e.target.name]: value,
            [e.target.name]: value,
            [e.target.name]: value
        }

        this.setState(({register}) => ({
            register: obj
        }))

    }


    sendData = (e) => {
        e.preventDefault();
        // window.location = '/#submit'

        const { password, passwordRepeat, email } = this.state.register;

        const areSame = password === passwordRepeat;

        if (areSame) {
            if (!email.includes('@') || !email.includes('.')) {
                return toastr.warning('Please type the proper email address.')
            }  else {
                const auth = this.todoService.authUser('/signup', this.state.register).then(body => {
                    const {status, text} = body;

                    if (status === "failure" && text === "registered:true") {
                        // TODO set localstorage or smth to prevent further actions with the system
                        return toastr.warning('Such email is already registered, please Log in to your account.');
                    }  else {
                        toastr.success('User has been registered. Please Log in to your account.')
                        // window.location = '/auth'
                        console.log(body);
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000)
                    }
                })
                console.log(auth);
            }

        } else {
            return toastr.error('Passwords should be the same.')
        }

    }

    render() {

        const { register } = this.state;

        return (
            <>
                <SingUp values={this.setValue} states={register} data={this.sendData} />
            </>
        )
    }

}


export const SingUp = ({values, states, data}) => {
    const { name, surname, email, password, passwordRepeat, inviteID } = states;

    return(


        <React.Fragment>
            <div className={loginModule['overlay']}></div>
            <div className={`${loginModule['register-container']} animate__animated animate__backInDown `}>
                <div className={`${loginModule['child']} ${loginModule['signup-thumbnail']}`}>
                    <div className={loginModule['logo']}>
                        <h1 className={loginModule['logo-text']}>Store | Admin Tools </h1>
                    </div>
                    <div className={`${loginModule['content']} center`}>
                        <h1 className={loginModule['primary']}>Create an account</h1>
                        <h4 className={loginModule['secondary']}>Please select the required fields.</h4>
                    </div>
                    <div className={loginModule['overlay-2']}></div>
                </div>
                <div className={`${loginModule['child']}  ${loginModule['signup']}`}>
                    <form action='#' onSubmit={ data }>
                        <div className={`${loginModule['group']} form-group`}>
                            <label htmlFor="username">Name</label>
                            <input className={`${loginModule['control']} form-control`} type="text" name="name" value={name} onChange={ values } id="name" placeholder="Name" required />
                        </div>
                        <div className={`${loginModule['group']} form-group`}>
                            <label htmlFor="username">Surname</label>
                            <input className={`${loginModule['control']} form-control`} type="text" name="surname" value={surname} onChange={ values } id="name" placeholder="Surname" required />
                        </div>
                        <div className={`${loginModule['group']} form-group`}>
                            <label htmlFor="email">Email</label>
                            <input className={`${loginModule['control']} form-control`} type="text" name="email" value={email} onChange={ values } id="email" placeholder="Email" required />
                        </div>
                        <div className={`${loginModule['group']} form-group`}>
                            <label htmlFor="password">Password</label>
                            <input className={`${loginModule['control']} form-control`} type="password" name="password" value={password} onChange={ values } id="password" placeholder="********" required />
                        </div>
                        <div className={`${loginModule['group']} form-group`}>
                            <label htmlFor="passwordRepeat">Repeat Password</label>
                            <input className={`${loginModule['control']} form-control`} type="password" name="passwordRepeat" value={passwordRepeat} onChange={ values } id="passwordRepeat" placeholder="********" required />
                        </div>
                        {/*<div className={`${loginModule['group']} form-group`}>*/}
                        {/*    <label htmlFor="avatar">Avatar</label>*/}
                        {/*    <input className={`${loginModule['control']} form-control`} type="text" name="avatar" value={avatar} onChange={ values } id="avatar" placeholder="Avatar" />*/}
                        {/*</div>*/}
                        <div className={`${loginModule['group']} form-group`}>
                            <label htmlFor="inviteID">InviteID</label>
                            <input className={`${loginModule['control']} form-control`} type="password" name="inviteID" value={inviteID} onChange={ values } id="inviteID" placeholder="********" required />
                        </div>
                        <div className="m-t-lg">
                            <ul className={`${loginModule['inline']} list-inline`}>
                                <li>
                                    <input className={`${loginModule['btn-form']} btn`}  type="submit" value="Register" />
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    )
}