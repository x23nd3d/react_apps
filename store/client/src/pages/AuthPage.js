import React, {Component} from "react";
import Login from "../components/login-panel";
import loginModule from '../components/login-panel/Login.module.scss';

export default class AuthPage extends Component{
    constructor() {
        super();

        this.state = {

        }



    }

    componentDidMount() {


    }


    render() {

        const {setAPI, login} = this.props;

        const overlay = <div className={loginModule['overlay']}></div>;

        return (
            <React.Fragment>
                {overlay}
                <Login setAPI={setAPI} login={login}/>
            </React.Fragment>
        )
    }


}

