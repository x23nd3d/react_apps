import React from "react";
import TodoApp from "../components/todo-app";
import UserWrapper from "../../../components/userWrapper";

export const TodoDashboard = ({logout, active}) => {

    return sessionStorage.getItem('API') ? <UserWrapper><TodoApp
        logout={logout}
        active={active}
        /></UserWrapper> : null;
}

