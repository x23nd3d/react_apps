import React, {Component} from 'react';
import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFiler from '../item-status-filter';
import AddNewItem from "../add-new-item";
import TodoService from "../../services/todoService";
import Spinner from "../spinner";
import './todo-app.css'
import main from "../../../../components/main/Main.module.scss";
import ProgressBar from "../../../../components/ProgressBar/ProgressBar";

// TODO: ADD REACT ROUTER, ADD METHODS WITH TODOS + ADD DATE OF ANY TODO AND MORE FEATURES

export default class TodoApp extends Component {
    constructor() {
        super();

        this.todoService = new TodoService();

        this.state = {
            data: <Spinner/>,
            todoData: [],
            term: '',
            filter: localStorage.getItem('filter') || "all",
        }

        this.getData = () => {
            this.todoService.getItems().then(body => {
                console.log(body, 'BODYDDD');
                const todoData = body.todos;
                if (todoData.length > 0) {
                    this.setState({todoData});
                } else {
                    // this.changeStatus();
                    this.changeStatus();
                    console.log(todoData, 'no TodoData')
                }

            })
        }

        this.toggleProperty = (array, id, prop) => {
            const idx = array.findIndex(item => item._id === id);
            const oldItem = array[idx];
            const newItem = {
                ...oldItem,
                [prop]: !oldItem[prop]
            };

            const updatedArray = [
                ...array.slice(0, idx),
                newItem,
                ...array.slice(idx + 1)
            ];

            this.todoService.refreshProperty(id, prop);


            return updatedArray;


        };

        this.onToggleDone = (id) => {
            this.setState(({todoData}) => {
                return {
                    todoData: this.toggleProperty(todoData, id, 'done')
                }
            });
        };

        this.onToggleImportant = (id) => {
            this.setState(({todoData}) => {
                return {
                    todoData: this.toggleProperty(todoData, id, 'important')
                }
            });
        };

        this.removeItem = (id) => {
            console.log(id, 'ID')
            this.todoService.removeItem(id).then(body => {
                const todoData = body.todos;
                if (todoData) {
                    this.setState({todoData});
                } else {
                    console.log(todoData, 'no TodoData')
                }
            })
        };

        this.search = (term, array) => {

            if (!term) {
                return array
            }

            return array.filter(item => {
                return item.label.toLowerCase().indexOf(term.toLowerCase()) > -1
            });
        };

        this.updateTerm = (term) => {
            this.setState({term})
        };

        this.addItem = (label) => {
            const newItem = this.createTodo(label);
            this.todoService.addItem(newItem).then(body => {
                // TODO: check why multiple items added + correct state, state todoData should get all items every time
                    const todoData = body.todos;
                console.log(body, 'DATA FROM SERVER');
                this.setState({todoData});
                 })
        };

        this.filter = (state, items) => {
            switch (state) {
                case 'all':
                    localStorage.setItem('filter', 'all');
                    return items;
                case 'active':
                    localStorage.setItem('filter', 'active');
                    return items.filter(item => !item.done);
                case 'done':
                    localStorage.setItem('filter', 'done');
                    return items.filter(item => item.done);
                default:
                    return items;
            }

        };

        this.updateFilter = (filter) => {
            this.setState({filter})
        };

        this.changeStatus = () => {
            this.setState({data: <div className="noData">No items found</div>});
        }


    }



    createTodo(label) {
        return {
            label,
            done: false,
            important: false,
        }
    }

    componentDidMount() {
        this.getData()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.todoData !== this.state.todoData) {
            if (!this.state.todoData.length) {
                this.changeStatus();
            }
        }
    }

    render() {

        const { data, todoData, term, filter } = this.state;

        const hasData = todoData.length > 0 ? true : null;

        console.log(todoData, 'TODODATA');

        let doneItems = 0;
        let todoItems = 0;

        let todos = null;

            if (hasData) {

                const visibleItems = this.filter(filter, this.search(term, todoData));

                doneItems = todoData.filter(item => item.done).length;
                todoItems = todoData.length - doneItems;

                todos = <TodoList todos={visibleItems} toggleDone={this.onToggleDone} toggleImportant={this.onToggleImportant} onRemove={this.removeItem}/>

            } else {
                todos = data
                // TODO check if was connected after all, because may be issues with the server, then another error show
            }


        console.log(this.props, 'TODO PROPS');


        return (
            <>
          {/*<Sidebar user={user} />*/}
                <div className={main['container']}>
                    {/*<Navbar logout={this.props.logout} active={this.props.active} user={user} />*/}
          <ProgressBar>
            <div className="todo-app">
                <AppHeader todo={todoItems} done={doneItems}/>
                <div className="top-panel d-flex">
                <SearchPanel onSearch={this.updateTerm}/>
                <ItemStatusFiler
                    updateFilter={this.updateFilter}
                    filter={filter}/>
                </div>
                {todos}
                <AddNewItem onAddItem={this.addItem}/>
            </div>
          </ProgressBar>
                </div>

            </>
        )
    }


};

