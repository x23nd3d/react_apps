export default class User {

    _apiBase = 'http://176.104.6.131:3000/api/auth';


    async getUser() {
        const res = await fetch(`${this._apiBase}/user`, {
            method: 'GET',
            headers: {
                "Authorization": `${sessionStorage.getItem('API')}`
            }
        });

        if (!res.ok) {
            throw new Error(`Could not fetch ${this._apiBase}/user` +
                `, received ${res.status}`)
        }
        return await res.json();

    }


    async changeLang(lang) {
        const res = await fetch(`${this._apiBase}/chlang`, {
            method: 'POST',
            body: JSON.stringify(lang),
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `${sessionStorage.getItem('API')}`
            }
        });

        if (!res.ok) {
            throw new Error(`Could not fetch ${this._apiBase}/chLang` +
                `, received ${res.status}`)
        }
        return await res.json();

    }
    //
    // async removeItem(id) {
    //     const obj = {
    //         id
    //     }
    //     const res = await fetch(`${this._apiBase}/${id}/remove`, {
    //         method: 'DELETE',
    //         body: JSON.stringify(obj),
    //         headers: {
    //             'Content-Type': 'application/json',
    //             "Authorization": `${sessionStorage.getItem('API')}`
    //         }
    //     });
    //
    //     if (!res.ok) {
    //         throw new Error(`Could not fetch ${this._apiBase}/${id}/remove` +
    //             `, received ${res.status}`)
    //     }
    //     return await res.json();
    //
    // }
    //
    // async getItems() {
    //     const res = await fetch(`${this._apiBase}/show`, {
    //         method: 'GET',
    //         headers: {
    //             "Authorization": `${sessionStorage.getItem('API')}`
    //         }
    //     });
    //
    //     if (!res.ok) {
    //         throw new Error(`Could not fetch ${this._apiBase}/add` +
    //             `, received ${res.status}`)
    //     }
    //     return await res.json();
    //
    // }
    //
    //
    // async refreshProperty(id, prop) {
    //
    //     const obj = {
    //         id,
    //         prop
    //     }
    //     const res = await fetch(`${this._apiBase}/update`, {
    //         method: 'POST',
    //         body: JSON.stringify(obj),
    //         headers: {
    //             'Content-Type': 'application/json',
    //             "Authorization": `${sessionStorage.getItem('API')}`
    //         }
    //     });
    //
    //     if (!res.ok) {
    //         throw new Error(`Could not fetch ${this._apiBase}/update` +
    //             `, received ${res.status}`)
    //     }
    //     return await res.json();
    //
    // }

}