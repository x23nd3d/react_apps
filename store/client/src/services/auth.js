export default class Auth {

    _apiBase = 'http://176.104.6.131:3000/api/auth';

    async authUser(url, data) {
        const res = await fetch(`${this._apiBase}${url}`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();
    }

    async checkIfAuth() {
        const res = await fetch(`${this._apiBase}/check`, {
            method: 'POST',
            headers: {
                "Authorization": `${sessionStorage.getItem('API')}`
            }
        });
    // TODO: error boundary
        if (!res.ok) {
            throw new Error(`Could not fetch ${sessionStorage.getItem('API')}` +
                `, received ${res.status}`)
        }
        return await res.json();
    }

    async userAttempt() {
        const res = await fetch(`${this._apiBase}/attempt`, {
            method: 'GET'
        });
        if (!res.ok) {
            throw new Error(`Could not fetch ${sessionStorage.getItem('API')}` +
                `, received ${res.status}`)
        }
        return await res.json();

    }

    async logOut() {
        const res = await fetch(`${this._apiBase}/logout`, {
            method: 'POST',
            headers: {
                "Authorization": `${sessionStorage.getItem('API')}`
            }
        });
        // TODO: error boundary
        if (!res.ok) {
            throw new Error(`Could not fetch ${sessionStorage.getItem('API')}` +
                `, received ${res.status}`)
        }
        return await res.json();
    }
}