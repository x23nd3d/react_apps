import React from "react";
import {Switch, Route, Redirect} from 'react-router-dom'
import AuthPage from "./pages/AuthPage";
import Dashboard from "./pages/Dashboard";
import {Register} from "./pages/Register";
import {TodoDashboard} from "./applications/todo/pages/TodoDashboard";
import {setToken} from "./components/app/useToken";
import NoMatch from "./pages/NoMatch";


export const Routes = (isAuthenticated, accessible, logout, connected, login) => {

    console.group('isAuthenticated', isAuthenticated, 'accessible', accessible, 'connected', connected);
    console.groupEnd()

    if (isAuthenticated) {
        return (
            <Switch>
                <Route path="/dashboard" exact >
                    <Dashboard logout={logout} active={connected}/>
                </Route>
                <Redirect from="/auth" to="/dashboard"/>
                {/*<Redirect to="/dashboard"/>*/}
                {/*<Redirect to="/dashboard"/>*/}
                {/*<Redirect to="/dashboard"/>*/}
                {/*<Route path="/register" exact>*/}
                {/*    <Register/>*/}
                {/*</Route>*/}
                <Route path="/todo" >
                        <TodoDashboard logout={logout} active={connected}/>
                </Route>
                <Redirect from="/dashboard" to="/todo"/>
                {/*<Redirect to="/register"/>*/}
                {/*<Redirect to="/dashboard"/>*/}
                <Route>
                    <NoMatch />
                </Route>
            </Switch>

        )
    } else {
        if (!accessible) {
            return (
                <Switch>
                    <Route path="/forbidden" exact>
                        <div>ACCESS NOT ALLOWED</div>
                    </Route>
                    <Redirect to="/forbidden"/>
                </Switch>
            )
        } else {

            return (
                <Switch>
                    <Route path="/auth" exact>
                        <AuthPage setAPI={setToken} login={login}/>
                    </Route>
                    <Redirect to="/auth"/>
                    <Route path="/register" exact>
                        <Register/>
                    </Route>
                </Switch>
            )
        }
    }




}
