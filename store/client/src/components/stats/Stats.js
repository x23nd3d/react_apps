import React from 'react';
import stats from './Stats.module.scss';

export default function Stats({title, count, data, children}) {

    const graphs = data.map((item, i) => {
        return (
            <div key={i} className={stats['graph']} style={{height:item + 'px'}}></div>
        )
    });

        return(
            <>
                <div className={stats['main__stats']}>
                    <p className={stats['title']}>{title}</p>
                    <div className={stats['numbers']}>
                        <p className={stats['counter']}>{count}</p>
                        <div className={stats['goals']}>
                            <span className={`material-icons MuiIcon-root text-green ${stats['arrow']}`} aria-hidden="true">trending_up</span>
                            <p className={stats['target-number']}>13% of target</p>
                        </div>
                    </div>

                    <div className={stats['graphs']}>
                        {  children ? children : graphs  }
                    </div>
                </div>
            </>
        )
}
