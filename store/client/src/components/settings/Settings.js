import React, {Component} from 'react';
import settings from './Settings.module.scss';
import OverlayWrapper from "../hoc/overlay";


export default class Settings extends Component {

    state = {
        shown: false
    }

    render() {

        return (
            <>
                <OverlayWrapper funcDestroy={this.props.funcDestroy}><MainSettings/></OverlayWrapper>
            </>
        )
    }
}

const MainSettings = ({clazz}) => {


    const clazzes = clazz ? clazz : 'animate__fadeInDown';
    return (
        <>


            <div className={`${settings['main']} animate__animated ${clazzes} `}>

            </div>

        </>
    )
}


// const ViewSettings = () => {
//
//     return (
//         <>
//             <div className={settings['view']}>
//
//             </div>
//         </>
//     )
// }
//
// const NotificationsSettings = () => {
//
//     return (
//         <>
//             <div className={settings['notifications']}>
//
//             </div>
//         </>
//     )
// }
//
// const AppsSettings = () => {
//
//     return (
//         <>
//             <div className={settings['apps']}>
//
//             </div>
//         </>
//     )
// }