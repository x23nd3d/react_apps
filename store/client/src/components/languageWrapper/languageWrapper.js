import React, {Component} from 'react';
import './languageWrapper.css';

export default class LanguageWrapper extends Component {

    state = {
        lang: 'en'
    }


    // userDetails = () => {
    //     return this.user.getUser().then(body => body.user);
    // }
    //
    // componentDidMount() {
    //     this.userDetails().then(user => {
    //         this.setState(({user}));
    //     });
    // }


    render() {

        const { lang } = this.state;

        console.log('React.Children', React.Children)

        const children = React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
                lang
            });
        })


        const app = lang ? children : null;

        return (
            [app]
        )
    }
}
