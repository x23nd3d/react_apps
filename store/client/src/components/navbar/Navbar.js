import React, {Component} from 'react';
import '@fortawesome/fontawesome-free';
import nav from './Navbar.module.scss';


export default class Navbar extends Component {

    state = {
        dropdown: false,
        clazz: null
    }


    renderDropdown = () => {

        this.setState(({dropdown}) => {
            return {
                dropdown: !dropdown
            }
        });


        if (!this.state.dropdown) {
            document.addEventListener('click', this.renderTest, false);
        }
    }

    renderTest = (e) => {

        if (this.node.contains(e.target)) {
            return;
        }

        this.renderDropdown();
        document.removeEventListener('click', this.renderTest, false);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.dropdown !== this.state.dropdown) {
            this.setState(({clazz}) => {
                const newClazz = this.state.dropdown ?
                    `animate__fadeIn ${nav['secIn']}` :
                    `animate__fadeOut ${nav['secOut']}`;

                return {
                    clazz: newClazz
                }
            });

            // if (!this.state.dropdown) {
            //     setTimeout(() => {
            //         this.setState(({clazz}) => {
            //             const newClazz = null
            //             return {
            //                 clazz: newClazz
            //             }
            //         });
            //     }, 200)
            // }


        }
    }


    render() {

        const { dropdown, clazz } = this.state;
        const { logout, user, settings, lang } = this.props;


        const dropStyle = dropdown ? nav['user-info-checked'] : ""

        return(
            <>
                <section className={nav['main__navbar']}>
                    <div className={nav['tools']}>
                        <ul className={nav['tools-list']}>
                            <li className={nav['tool']}>
                                <i className="far fa-calendar-plus"></i>
                            </li>
                            <li className={nav['tool']}>
                                <i className="far fa-envelope"></i>
                            </li>
                            <li className={nav['tool']}>
                                <i className="far fa-address-book"></i>
                            </li>
                            <li className={nav['tool']}>
                                <i className="far fa-check-square"></i>
                            </li>
                        </ul>
                    </div>

                    <div className={nav['base']}>

                        <div className={nav['actions']}>
                            <span onClick={(e) => lang(e.target.textContent.toLowerCase())} className={`${nav['lang']} ${nav['action']}`}>RU</span>
                            <span onClick={(e) => lang(e.target.textContent.toLowerCase())} className={`${nav['lang']} ${nav['action']}`}>EN</span>
                            <div className={nav['action']}>
                                <i className="fas fa-plus"></i>

                            </div>

                            <div className={nav['action']}>
                                <i className="fas fa-search"></i>
                            </div>

                            <div className={nav['action']}>
                                <i className="fas fa-list list"></i>
                            </div>

                        </div>

                        <div ref={node => { this.node = node; }} className={`${nav['user-info']} ${dropStyle}`} onClick={this.renderDropdown.bind(this)}>
                            <div className={nav['user-credentails']}>
                                <p className={nav['user-name']}>{`${user.name} ${user.surname.slice(0, 1).toUpperCase() + '.'}`}</p>
                                <p className={nav['user-role']}>{`${user.role[0].toUpperCase()}${user.role.slice(1)}`}</p>
                            </div>
                            <img src={user.avatar} alt="user-avatar" className={nav['user-img']}/>

                        </div>

                        {
                            clazz ? <DropdownMenu
                                menu={['Settings', 'Log out']}
                                clazz={clazz}
                                logout={logout}
                                settings={settings}
                                removeListener={this.renderTest}/> : null
                        }

                    </div>

                </section>
            </>
        )
    }
}

const DropdownMenu = ({menu, clazz, logout, settings, removeListener}) => {

    const items = menu.map((item, i) => {


        const onlick = item === 'Log out' ? logout.bind(this, removeListener, clazz) : settings.bind(this)

        return (
                <li key={i} onClick={onlick} className={`${nav[item.toLowerCase().replace(/\s+/g, "")]} ${nav['dropdown-item']}`}>{item}</li>
        )
    });



    return (
        <div className={`${nav['dropdown']} animate__animated ${clazz}`}>
            <ul className={nav['dropdown-menu']} >
                  {items}
            </ul>
        </div>
    )

}
