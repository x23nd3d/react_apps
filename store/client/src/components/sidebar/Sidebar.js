import React, {Component} from 'react';
import sidebar from './Sidebar.module.scss';
// import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import {Link} from "react-router-dom";



export default class Sidebar extends Component {


    render() {

        const { user, lang } = this.props;
        const l = lang();

        return(
            <>
                <div className={sidebar['main__sidebar']}>

                    <div className={sidebar['header']}>

                        <div className={sidebar['title']}>
                        <p className={sidebar['logo']}>{l.store}</p>
                        </div>

                        <div className={sidebar['user']}>
                            <div className={sidebar['user-info']}>
                                <p className={sidebar['user-name']}>{`${user.name} ${user.surname}`}</p>
                            </div>

                            <div className={sidebar['user-avatar']}>
                                <img src={user.avatar} alt="sidebar-avatar" className={sidebar['user-img']}/>
                            </div>
                        </div>

                    </div>

                    <div className={sidebar['bottom']}>
                        <p className={sidebar['bottom-info']}>Menu</p>
                        <ul className={sidebar['bottom-list']}>
                                 <Link to="/dashboard">
                                     <li className={`${sidebar['bottom-item']} ${sidebar['bottom-link']} ${sidebar['dashboard']}`}>Dashboard </li>
                                 </Link>
                            <Link to="/users">
                                <li className={`${sidebar['bottom-item']} ${sidebar['bottom-link']} ${sidebar['users']}`}>Users </li>
                            </Link>
                                <Link to="/todo">
                                    <li className={`${sidebar['bottom-item']} ${sidebar['bottom-link']} ${sidebar['check']}`}>Todo </li>
                                </Link>
                             <Link to="/calendar">
                            <li className={`${sidebar['bottom-item']} ${sidebar['bottom-link']} ${sidebar['calendar']}`}>Calendar </li>
                        </Link>
                        </ul>
                    </div>

                </div>
            </>
        )
    }
}
