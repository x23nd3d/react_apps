import React, {Component} from 'react';
import './UserWrapper.css';
import User from "../../services/User";

export default class UserWrapper extends Component {

    state = {
        user: ''
    }

    user = new User();

    userDetails = async () => {
        return  await this.user.getUser().then(body =>  body.user);

    }

    // CHECK WITHOUT ASYNC

    async componentDidMount() {
        await this.userDetails().then(user => {
            console.log('USER from WRAPPER', user)
            this.setState(({user}));
        });
    }


    render() {

        const { user } = this.state;

        const children = React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
                user
            });
        })


        const app = user ? children : null

        return (
            [app]
        )
    }
}
