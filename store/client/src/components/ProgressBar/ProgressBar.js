import {Component} from 'react';
import './ProgressBar.css';
const Nanobar = require('nanobar');

export default class ProgressBar extends Component {

    state = {
        loaded: false
    }

    componentDidMount() {

        let options = {
            id: 'nanobar',
        };
        const nanobar = new Nanobar( options );

        nanobar.go(100);

            setTimeout(() => {
                this.setState(({loaded}) => {
                    return {
                        loaded:!loaded
                    }
                });
                this.selfDestroy();
            }, 500)
        }





    selfDestroy = () => {
            document.getElementById('nanobar').remove();
    }

    render() {

        const { loaded } = this.state;

        const app = loaded ? this.props.children : null;

        return (
            [app]
        )
    }
}
