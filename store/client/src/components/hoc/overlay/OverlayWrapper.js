import React, {Component} from 'react';
import overlay from './OverlayWrapper.module.scss';
import settings from '../../settings/Settings.module.scss';

export default class OverlayWrapper extends Component {

    state = {
        active: true,
        clazz: null,
    }

    selfDestroy = (e, func) => {

        if (e.target.classList.contains(settings['main'])) {
            return
        }

        this.setState(({clazz}) => {
            const newClazz = "animate__bounceOutUp";
            return {
                clazz: newClazz
            }
        });


        setTimeout(() => {


            this.setState(({active}) => {
            return {
                active: !active
            }
             });

            //say to App that settings are inactive now
            func();


        }, 1000)




    }

    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     if (prevState.active !== this.state.active) {
    //         console.log('state changed')
    //         this.setState(({clazz}) => {
    //             const newClazz = this.state.active ?
    //                null :
    //                 "animate__bounceOutUp";
    //
    //             return {
    //                 clazz: newClazz
    //             }
    //         });
    //
    //         if (!this.state.active) {
    //             setTimeout(() => {
    //                 this.setState(({clazz}) => {
    //                     const newClazz = null
    //                     return {
    //                         clazz: newClazz
    //                     }
    //                 });
    //             }, 1000)
    //         }
    //
    //
    //     }
    // }


    render() {


        const { active, clazz } = this.state;

        console.log('OVERLAY WRAPPER ACTIVE', active)

        const children = React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
                clazz
            });
        })

        const app = clazz ? children : [this.props.children];
        const destroyed = clazz ? {pointerEvents: 'none'} : null;

        console.log('this funcDestroy', this.props.funcDestroy);


        if (active) {
            return (
                <div style={destroyed} className={overlay['main']} onClick={(e) => this.selfDestroy(e, this.props.funcDestroy)}>
                    {[app]}
                </div>
            )
        } else {
            return null
        }



    }
}
