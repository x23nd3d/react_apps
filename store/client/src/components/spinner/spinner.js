import React from 'react';
import './spinner.css';

const Spinner = () => {
    return (
        <div id="preloader">
            <div id="loader"></div>
        </div>
    )
}

export default Spinner;