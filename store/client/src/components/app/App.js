import React, {Component} from 'react';
import {CreateStore} from 'redux';
import {BrowserRouter as Router, Redirect} from 'react-router-dom'
import Auth from "../../services/auth";
import {Routes} from "../../routes";
import 'reset-css';
import appScss from './App.module.scss'
import '@fortawesome/fontawesome-free/css/all.css';
import 'toastr/build/toastr.css';
import * as toastr from 'toastr';
import Spinner from "../spinner";
import User from "../../services/User";
import Navbar from "../navbar";
import Sidebar from "../sidebar";
import Settings from "../settings/Settings";
import text from "../../lang/languages.json";
import {checkToken} from "./useToken";
require('typeface-montserrat');
// import '@fortawesome/fontawesome-free/js/all.js';
// import Main from "../main";

// import 'bootstrap/dist/css/bootstrap.css';
// require('bootstrap');

// require('materialize-css')

// TODO API should be checked through the server, if the key === API session storage, then log in, else = fuck off


export default class App extends Component {

    //
    // constructor() {
    //     super();

        state = {
            isAuth: false,
            connected: false,
            access: null,
            loading: true,
            settings: false,
            language: 'not set',
            user: 'guest',
        }



        auth = new Auth();
        user = new User();

        attemptUser = async () => {
            await this.auth.userAttempt().then(async body => {

                const { status } = body;

                if (status === 'not_allowed_ip_rule') {
                     this.setState({access: false});
                } else {
                     this.setState({access: true});

                }
            });

        }

        receiveUser = async () => {
            const receivedUser = await this.user.getUser().then(data => {
                if (data) {
                    return  data.user;
                } else {
                    return 'guest'
                }
            });

            this.setState(({user}) => {
                return {
                    user: receivedUser
                }
            })
        }


        logOut = async (funct) => {
            await this.auth.logOut();
            sessionStorage.clear();
            localStorage.clear();
            toastr.info('Bye, bye!');
            document.removeEventListener('click', funct);
            this.setState(({isAuth}) => {
                return {
                    isAuth: !isAuth
                }
            });
            this.setState(({connected}) => {
                return {
                    connected: !connected
                }
            });
            this.setState({user: 'guest'})
        };

        checkIfConnected = () => {
            // TODO STOP CHECKING AFTER LOG OUT
                const checking = setInterval(() => {
                    this.auth.checkIfAuth().then(body => {
                        const { status } = body;

                        if (status === "connected") {
                            // this.setState({connected: true});
                            // clearInterval(checking);
                            // console.log('TRUE')
                        } else {
                            this.setState({connected: false});
                            // console.log('FALSE')
                            this.auth.logOut();
                            sessionStorage.removeItem('API');
                            this.setState({isAuth: false})

                            const listener = function () {
                                if (!document.hidden) {
                                    setTimeout(() => {
                                        toastr.warning('Your session was expired, please log in.');
                                    }, 1000)
                                }
                            }

                            document.addEventListener( 'visibilitychange' , listener, false);

                            document.addEventListener( 'visibilitychange' , function() {
                                if (document.hidden) {
                                    document.removeEventListener('visibilitychange', listener);
                                }
                            }, false );


                            clearInterval(checking);
                        }
                    })
                }, 5000)
            }

        toggleSettings = () => {
            // TODO settings does not open every time
            this.setState(({settings}) => {
                return {
                    settings: !settings
                }
            });
        }

        changeLanguage = () => {
            let current;
            for (let textKey in text) {
                if (textKey === this.state.language) {
                    current = text[textKey];
                }
            }
            return current;
        }

        switchLanguage = (lang) => {
            const sentLanguage = {language: lang}
           this.user.changeLang(sentLanguage).then(data => {
               this.setState(({language}) => {
                   return {
                       language: data.lang
                   }
               })
            })

        }

        logIn = async () => {
            this.setState({isAuth: true});
            await this.receiveUser();
            await this.setState({language: this.state.user.lang})
            this.setState({connected: true})
            this.checkIfConnected();

        }

         routes =  Routes(this.state.isAuth, this.state.access, this.logOut, this.state.connected, this.logIn);


    async componentDidMount() {

      await this.attemptUser();
      const isAuthenticated = checkToken();


        if (isAuthenticated) {
            this.setState({isAuth: isAuthenticated});
            await this.receiveUser();
            this.setState({connected: true})
            this.setState({language: this.state.user.lang})
            this.checkIfConnected();
        } else {
            console.log('THERE IS NO TOKEN, SO LET"S USE ROUTER AND REDIRECT');
        }

       if(!localStorage.getItem('spinnerLoaded')) {
           setTimeout(() => {
                   this.setState(({loading}) => {
                       return {
                           loading:!loading,
                       }
                   });
                   this.saveSpinnerLoaded();

           }, 1500)
       } else {
           this.setState(({loading}) => {
               return {
                   loading:!loading,
               }
           });
       }

       window.onbeforeunload = () => {
            this.clearSpinnerLoaded();
       }




    }

    saveSpinnerLoaded = () => {
        localStorage.setItem('spinnerLoaded', true);
    }

    clearSpinnerLoaded = () => {
        localStorage.removeItem('spinnerLoaded');
    }


     render() {

        const {isAuth, connected, access, settings, user } = this.state;

         this.routes = Routes(isAuth, access, this.logOut, connected, this.logIn);
         const body = document.getElementsByTagName('body')[0];
         body.style.backgroundColor = this.state.loading ? "black" : "unset";

         const areSettingsActive = settings ? <Settings funcDestroy={this.toggleSettings}/> : null


                if (this.state.loading) {
                    return <Spinner/>
                } else {
                    if (connected) {
                        return(
                            <>
                                <Router>
                                    {areSettingsActive}
                                   <Sidebar user={user} lang={this.changeLanguage} />
                                    <div className={appScss['container']}>
                                        <Navbar
                                            logout={this.logOut}
                                            active={isAuth}
                                            user={user}
                                            settings={this.toggleSettings}
                                            lang={this.switchLanguage}/>
                                        {this.routes}
                                    </div>

                                </Router>
                            </>
                        )
                    } else {
                        return (
                            <Router>
                                <Redirect to="/auth"/>
                                {this.routes}
                                </Router>
                        );
                    }

                }
    }
}
