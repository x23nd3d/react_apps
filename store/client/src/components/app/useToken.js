export const checkToken = () => {
    return !!sessionStorage.getItem('API');
}

export const setToken = (token) => {
    return sessionStorage.setItem('API', token);
}