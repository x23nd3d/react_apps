import React, {Component} from "react";
import '../../services/auth';
import '../../../../client/node_modules/animate.css/animate.css';
import loginModule from './Login.module.scss'
import Auth from "../../services/auth";
import * as toastr from 'toastr';
import 'bootstrap/dist/css/bootstrap.css';
require('bootstrap');


export default class Login extends Component {

    todoService = new Auth();

    state = {
        active: true,
        singIn: false,
        login: {
            email: "",
            password: ""
        }
    }

        changeMode = () => {
            this.setState(({singIn}) => {
                return {
                    singIn: !singIn
                }
            })
        }


    getValue = (e) => {
        const value = e.target.value;

        // TODO correct UNDEFINED AND MAKE AS 1 FUNCTION
        const obj = {
            ...this.state.login,
            [e.target.name]: value,
            [e.target.name]: value,
        }

        this.setState(({login}) => ({
            login: obj
        }))

}



        getData = (e) => {
            e.preventDefault();
            const { email } = this.state.login;

            if (!email.includes('@') || !email.includes('.')) {
                return toastr.warning('Please type the proper email address.')
            } else {
                const auth = this.todoService.authUser('/login', this.state.login).then(body => {
                    const { status, text } = body;
                    console.log(status, text)
                    if (status === "failure" && text === "notfound") {
                        return toastr.warning('Such user does not exist. Please try again.')
                    } else if(status === "failure" && text === "datawrong") {
                        return toastr.error('Email or Password are incorrect. Please try again.')
                    } else {

                        const { status, token, userId: { name } } = body;
                        const { setAPI, login } = this.props;

                        if (status === "Success" && token !== undefined) {
                            toastr.success(`Welcome on board, ${name}`);
                            setTimeout(  async () => {
                                setAPI(token);
                                await login();
                            }, 800)


                            // TODO add info to cookies or sessionStorage or somewhere to save this info
                        }
                        console.log(body, 'USERR')





                    }

                })
                console.log(auth, 'AUTHH');
            }


        }



    render() {

        const { login } = this.state;

        let content =  <SignIn values={this.getValue} states={login} data={this.getData}/>;

        // let content;
        // let btnInfo;
        //
        //
        // if (singIn) {
        //     btnInfo = "I have an account"
        //     content = <h3 style={{color: '#000'}}>Soon.</h3>
        // } else {
        //     btnInfo = "Request access"
        //     content =  <SignIn values={this.getValue} states={login} data={this.getData}/>
        // }

    return(

    <React.Fragment>
        <div className={`${loginModule['container']} animate__animated animate__backInDown `} >
            <div className={`${loginModule['child']}  ${loginModule['signup-thumbnail']}`}>
                <div className={loginModule['logo']}>
                    <h1 className={loginModule['logo-text']}>Store | CRM</h1>
                </div>
                <div className={`${loginModule['content']} center`}>
                    <h1 className={loginModule['primary']}>Welcome to the application.</h1>
                    <h4 className={loginModule['secondary']}>Please log in for further actions.</h4>
                </div>
                {/*<ul className={`${loginModule['inline']} list-inline`}>*/}
                {/*    <li>*/}
                {/*        <a id={loginModule['go']} href="#" className={`${loginModule['signup-link']} btn`} onClick={this.changeMode}>{btnInfo}</a>*/}
                {/*    </li>*/}
                {/*</ul>*/}
                <div className={loginModule['overlay-2']}></div>
            </div>
            <div className={`${loginModule['child']} ${loginModule['signup']} `}>
                {content}
            </div>
        </div>
    </React.Fragment>
        )

        }


}

const SignIn = ({ values, states, data }) => {
    const { email, password } = states;
    return(
        <React.Fragment>
                <form action="#" onSubmit={ data } className={loginModule['signInForm']}>
                    <div className={`${loginModule['group']} form-group`}>
                        <label htmlFor="email">Email</label>
                        <input className={`${loginModule['control']} form-control`} type="text" name="email" value={email} onChange={ values } id="email" placeholder="Your email" required />
                    </div>
                    <div className={`${loginModule['group']} form-group`} >
                        <label htmlFor="password">Password</label>
                        <input className={`${loginModule['control']} form-control`} type="password" name="password" value={password} onChange={ values } id="password" placeholder="********" required />
                    </div>
                    <div className="m-t-lg">
                        <ul className={`${loginModule['inline']} list-inline`}>
                            <li>
                                <input className={`${loginModule['btn-form']} btn`} type="submit" value="Sign in" />
                            </li>
                        </ul>
                    </div>
                </form>
        </React.Fragment> )
}



