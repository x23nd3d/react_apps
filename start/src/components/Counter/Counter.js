import React, {Component} from 'react';

export default class Counter extends Component {
    state = {
        counter: 0
    }

    addCount = () => {
        this.setState({
            counter: this.state.counter + 1
        });
    }

    render() {
        return (
            <>
                <h2>Counter {this.state.counter}</h2>
                <button onClick={this.addCount}>+</button>
                <button onClick={() => this.setState({counter: this.state.counter - 1})}>-</button>
            </>
        )
    }
}
