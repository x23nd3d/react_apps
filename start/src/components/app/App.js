import React, {Component} from 'react';
import Car from "../testing/Testing"
import './app.css';

export default class App extends Component {

    state = {
        mainTitle: "React Components",
        cars: [
            { name: "Mazda", year: 2018 },
            { name: "Renault", year: 2018 },
        ],
        showCars: true
    }

    changeTitle = (name, index) => {
        const car = this.state.cars[index];
        car.name = name;
        const cars = [...this.state.cars]
        this.setState({
            cars
        });
    }

    toggleCars = () => {
        this.setState(({showCars}) => {
            return {
                showCars: !showCars
            }
        })
    };

    removeCar = (idx) => {
        const cars = this.state.cars;
        const newCarlist = [
            ...cars.slice(0, idx),
            ...cars.slice(idx + 1)
        ];

        this.setState({
            cars: newCarlist
        });
    }


    render() {

        const { cars, showCars } = this.state;

        const allCars = cars.map(({name, year}, i) => {
            return(
                <Car name={name} year={year} key={i} change={(e) => this.changeTitle(e.target.value, i)} remove={this.removeCar.bind(this, i)}/>
            )
        });

        const visible = showCars ? allCars : null;



        return (
            <>
            <button type="submit" onClick={this.toggleCars}>Toggle cars</button>
            {visible}
            </>
        );

  }
}
