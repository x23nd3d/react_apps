import React, {Component} from 'react';
import './testing.css'

export default class Car extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('shouldUpdate check', nextProps, nextState);
        return nextProps.name.trim() !== this.props.name.trim();
    }

    componentWillUnmount() {
        console.log('will unmount')
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     console.log('car getDerivedStateFromProps', nextProps, prevState);
    //     return prevState;
    // }

    // getSnapshotBeforeUpdate(prevProps, prevState) {
    //     console.log('car getSnapshotBeforeUpdate')
    // }

    render() {

        const {name, year, change, remove} = this.props;

        const clazz = ["input"];

        if (name.length >= 5) {
            clazz.push("bold", "green")
        } else if (!name.length) {
            clazz.push("red")
        } else {
            clazz.push("green")
        }



        return (
            <>
                <h1>Model: {name}</h1>
                <h3>Year: {year}</h3>
                <input type="text" className={clazz.join(' ').trim()} value={name} onChange={change}/>
                <button type="submit" onClick={remove}>Delete</button>
            </>
        )
    }
}

