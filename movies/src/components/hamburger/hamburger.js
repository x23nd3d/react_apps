import React from 'react';
import './hamburger.css';

const Hamburger = () => {
    return (
        <div className="main-part">
            <div className="line-1">
                <div className="line-2">
                    <div className="line-3"></div>
                </div>
            </div>
        </div>
    )
}

export default Hamburger;