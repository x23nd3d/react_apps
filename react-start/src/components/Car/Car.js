import React from "react";
import './Car.css';

const Car = ({name, year, change, input, current, deleteIt}) => {

    const dynamicCss = [];

    if (!name) {
        dynamicCss.push('red');
    } else {
        dynamicCss.push('green');
    }

    if (name.length < 5) {
        dynamicCss.push('small');
    } else {
        dynamicCss.push('bold');
    }

    return(
        <>
            <h3>Car name: {name}</h3>
            <p>Year: <strong>{year}</strong></p>
            <input className={dynamicCss.join(' ')} type="text" value={name} onChange={current}/>
            <button onClick={deleteIt}>Delete</button>
            {/*<input type="text" onChange={input}/>*/}
            {/*<button onClick={change}>Click</button>*/}
        </>
    )
}

export default Car