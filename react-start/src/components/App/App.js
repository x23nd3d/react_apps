import React, {Component} from "react";
import Car from '../Car/Car';

export default class App extends Component {

    state = {
        cars: [
            { name: "Ford", year: 2012},
            { name: "Mazda", year: 2021},
            { name: "Audi", year: 2020},
        ],
        pageTitle: "React components",
        toggle: true
    }

    changePageTitle = (newTitle) => {

        if (!newTitle.length) {
            console.log('No item put')
            const oldTitle = this.state.pageTitle;
            const pageTitle = oldTitle + ' (changed)';

            this.setState({
                pageTitle
            });
        } else {
            this.setState({
                pageTitle: newTitle
            });
        }
    }

    onInputChange = (e) => {
        const newValue = e.target.value;
        this.setState({
            pageTitle: newValue
        })
    }

    onInputChangeCurrent = (name, idx) => {
      const car = this.state.cars[idx];
      car.name = name;

      // const newCars = [
      //     ...this.state.cars.slice(0, idx),
      //     car,
      //     ...this.state.cars.slice(idx + 1)
      // ]
        const cars = [...this.state.cars];
        cars[idx] = car;

        console.log(cars);


        this.setState({
            cars
        });
    }

    toggleList = () => {
       this.setState(({toggle}) => {
           return {
                toggle: !toggle
           }
       })
    }

    deleteCar = (idx) => {
        // const cars = [
        //     ...this.state.cars.slice(0, idx),
        //     ...this.state.cars.slice(idx + 1)
        // ];

        // const newCar = this.state.cars[idx];
        // const cars = this.state.cars.filter(car => {
        //     return car !== newCar
        // });

        const cars = [...this.state.cars];
        cars.splice(idx, 1);

        console.log(cars);

        this.setState({
            cars
        })
    }


    render() {

        const { toggle } = this.state;

        const appStyle = {
            textAlign: 'center',
            fontSize: '36px'
        }

        const cars = toggle ?
            this.state.cars.map((item, i) => {

            return (
                <Car key={i} name={item.name} year={item.year}
                     // change={this.changePageTitle.bind(this, item.name)}
                     // input={this.onInputChange}
                     // current={this.onInputChangeCurrent.bind(this, i)}
                     current={e => this.onInputChangeCurrent(e.target.value, i)}
                     deleteIt={this.deleteCar.bind(this, i)}
                />
            )
        }) : null;

        return (
            <div style={appStyle}>
                <button onClick={this.toggleList}>Show cars</button>
                <h1 style={{fontSize:'60px'}}>{this.state.pageTitle}</h1>
                <button onClick={this.changePageTitle}>Change title</button>
                <div className='cars'>
                    {cars}
                </div>

            </div>
        );
    }


}

